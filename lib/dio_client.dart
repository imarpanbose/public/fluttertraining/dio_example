import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:dio_example/my_interceptor.dart';
import 'package:dio_example/post.dart';

class DioClient{
  late final Dio _dio;

  DioClient()
    :_dio=Dio(
      BaseOptions(
        baseUrl: 'https://jsonplaceholder.typicode.com',
        connectTimeout: const Duration(seconds: 5),
        receiveTimeout: const Duration(seconds: 3),
        responseType: ResponseType.json
      )
    )..interceptors.addAll([
      MyInterceptor()
    ]);

    CancelToken cancelToken=CancelToken();
    Future<List<Post>> getAllPosts() async {
      
      final response=await _dio.get('https://jsonplaceholder.typicode.com/posts',cancelToken: cancelToken);
      final json=response.data;
      return List<Post>.from(
        json.map((p)=>Post.fromJson(p))
      );
    }

}