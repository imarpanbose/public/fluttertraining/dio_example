import 'package:dio/dio.dart';

class MyInterceptor extends Interceptor{


  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    final options=err.requestOptions;
    print(options.method);
    print(options.baseUrl);
    print(options.path);
    print(err.error);
    print(err.message);
    print(err.response?.statusCode);
    super.onError(err, handler);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers['Authorization']='wweasd,nas,dkajdfkjwerbndsfvdsf';
    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    // TODO: implement onResponse
    super.onResponse(response, handler);
  }

}