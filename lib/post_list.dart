import 'package:dio_example/dio_client.dart';
import 'package:dio_example/post.dart';
import 'package:flutter/material.dart';

class PostList extends StatefulWidget {
  final DioClient dioClient=DioClient();
  Future<List<Post>?>? postList;
  PostList({super.key});

  @override
  State<PostList> createState() => _PostListState();
}

class _PostListState extends State<PostList> {

  @override
  void initState() {
    widget.postList=widget.dioClient.getAllPosts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future:  widget.postList,
      builder: (context,snapshot) {
        if(snapshot.connectionState==ConnectionState.done){
            return ListView.separated(
        separatorBuilder: (context,index)=> const Divider(color: Colors.black12,),
        itemCount: snapshot.data!.length,
        itemBuilder: (context, index) {
          Post post=snapshot.data![index];
          return ListTile(title:Text(post.title ?? '') ,subtitle: Text(post.body ?? ''),);
        },
      );
        }else{
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        
      }
      );
  }

  @override
  void dispose() {
    widget.dioClient.cancelToken.cancel();
    super.dispose();
  }
}